module Markdown exposing (Markdown, markdown, editor)
import Html exposing (Html)
import Parser exposing (Parser)

type alias Markdown ast msg
  = { parser : Parser ast
    , convert : ast -> Html msg
    }

markdown : Markdown ast msg -> String -> Maybe (Html msg)
markdown mark source =
  case Parser.run mark.parser source of
    Ok ast -> Just <| mark.convert ast
    Err err -> Nothing

editor : Html msg
editor = Html.text "Editor not implemented"

(=>) : Markdown a msg -> Markdown b msg -> String -> Maybe Html msg
(=>) m n source =
  case markdown m source of
    Just result -> Just result
    Nothing -> case markdown n source of
      Just result -> Just result
      Nothing -> Nothing
