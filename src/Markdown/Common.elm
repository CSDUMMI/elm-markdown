module Markdown.Common exposing (common, heading, link)
import Markdown exposing (Markdown)
import Parser exposing (Parser, succeed, getChompedString, chompWhile, (|.), (|=), symbol)
import Html exposing (Html)
import Html.Attributes as Attributes

line_spaces = getChompedString <| chompWhile (\c -> c == ' ' || c == '\t')
until_end_of_line = getChompedString <| chompWhile (\c -> c /= '\n')

--| Utility function to chomp a string until a certain string.
getStringUntil : Char -> Parser String
getStringUntil xs = getChompedString <| chompWhile (\c -> c /= xs)

-- Headings
type alias Heading = { level : Int, content : String }

--| Parser for markdown heading.
heading : Markdown Heading msg
heading = let getLevel = getChompedString  <| chompWhile (\c -> c == '#')
              conversion ast = h ast.level [] [Html.text ast.content]
          in  { parser = succeed (\level content -> Heading (String.length level) content)
                          |= getLevel
                          |. line_spaces
                          |= until_end_of_line
              , convert = conversion
              }

--| Utility function to create a heading.
h : Int -> List (Html.Attribute msg) -> List (Html msg) -> Html msg
h i = case i of
        1 -> Html.h1
        2 -> Html.h2
        3 -> Html.h3
        4 -> Html.h4
        5 -> Html.h5
        6 -> Html.h6
        _ -> Html.div

-- Links
type alias Link = { content : String, href : String }

link : Markdown Link msg
link = { parser = succeed Link
                      |. symbol "["
                      |= getStringUntil ']'
                      |. symbol "]"
                      |. line_spaces
                      |. symbol "("
                      |= getStringUntil ')'
                      |. symbol ")"
        , convert = (\l -> Html.a [Attributes.href l.href] [Html.text l.content])
        }

type alias Common = {
                    }
common : Markdown Common msg
common = {
         ,
         }
