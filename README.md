# Elm Markdown
## Module to parse, edit and extend markdown entirely in elm.
### Usage
Parse according to the Common Markdown
specification:
```elm
import Markdown
html = Markdown.markdown Nothing "# h1"
```

### Editor
```elm
import Markdown
editor_html = Markdown.editor Nothing "# h1"
```
This creates a WYSIWYG, that can be embedded
in HTML.

### Extending
You can extend / overwrite the Common Markdown
specification:
```elm
import Markdown exposing (markdown, Extension)
import Parse exposing (Parse, (|.), (|=), succeed, symbol, float, spaces)

type alias Video = { "alt" : String, "link" : String }

video : Extension
video = { parser = succeed Video
                    |. symbol "!vid["
                    |. spaces
                    |= getAlt
                    |. spaces
                    |. symbol "]"
                    |. spaces
                    |. symbol "("
                    |= getLink
                    |. symbol ")"
        ,
        }
  where getAlt = getChompedString <| chompWhile (== ']')
        getLink = getChompedString <| chompWhile (==')')
